/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattern.operations;

import com.Model.ProcessManager;
import com.db.DbConnection;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Aniket
 */
public class ClearData {
    public ClearData() {
        deleteFolderData();
        deleteCdriveFolderData();
        deleteRecords();
    }
    
    private void deleteFolderData() {
        String folder = "images";
        try {
            FileUtils.cleanDirectory(new File(folder));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    private void deleteCdriveFolderData() {
        String folder = new ProcessManager().getProcessPath().trim() +"/images";
        try {
            FileUtils.cleanDirectory(new File(folder));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    private void deleteRecords() {
        PreparedStatement ps = null;
        Connection conn = null;
        try {
            conn = new DbConnection().getConnection();
            ps = conn.prepareStatement("DELETE FROM CHAPTER_INFO");
            ps.executeUpdate();
            ps = conn.prepareStatement("DELETE FROM SUBJECT_INFO");
            ps.executeUpdate();
            ps = conn.prepareStatement("DELETE FROM QUESTION_INFO");
            ps.executeUpdate();
            ps = conn.prepareStatement("DELETE FROM OPTION_IMAGE_DIMENSIONS");
            ps.executeUpdate();
            ps = conn.prepareStatement("DELETE FROM IMAGERATIO");
            ps.executeUpdate();
            ps = conn.prepareStatement("DELETE FROM PRINTED_TEST_INFO");
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(conn != null)
                    conn.close();
                if(ps != null)
                    ps.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}

