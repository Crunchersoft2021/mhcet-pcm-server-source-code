/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

/**
 *
 * @author Aniket
 */
public class SwapData {
    
    public String getSwappedData(String data) {
        char[] keys = {'0','1','2','3','4','5','6','7','8','9','-',' '};
        char[] values = {'A','!','G','Q','?','Y','F','#','%','U','P','Z'};
        char[] temp1 = data.trim().toCharArray();
        char[] temp2 = new char[temp1.length];
        for(int i=0;i<temp1.length;i++) {  
            for(int j=0;j<keys.length;j++) {
                if(keys[j] == temp1[i]) {
                    temp2[i] = values[j];
                    break;
                }
            }
        }
        return String.valueOf(temp2);
    }
    
    public String getUnSwappedData(String data) {
        char[] keys = {'0','1','2','3','4','5','6','7','8','9','-',' '};
        char[] values = {'A','!','G','Q','?','Y','F','#','%','U','P','Z'};
        char[] temp1 = data.trim().toCharArray();
        char[] temp2 = new char[temp1.length];
        for(int i=0;i<temp1.length;i++) {  
            for(int j=0;j<values.length;j++) {
                if(values[j] == temp1[i]) {
                    temp2[i] = keys[j];
                    break;
                }
            }
        }
        return String.valueOf(temp2);
    }
    
    public static void main(String[] args) {
        System.out.println(new SwapData().getSwappedData("08-10-17"));
    }
}
