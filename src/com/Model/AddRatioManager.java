/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import com.bean.ImageRatioBean;
import com.bean.QuestionBean;
import com.db.DbConnection;
import com.db.operations.MasterImageRationOperation;
import com.db.operations.QuestionPaperOperation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Aniket
 */
public class AddRatioManager {

    private Connection conn;
    private PreparedStatement ps;
    private ResultSet rs;
    
    public AddRatioManager() {
        conn = new DbConnection().getConnection();
    }
    
    
    public void setRatio() {
        ArrayList<QuestionBean> totalQuesList = getMasterQuestionList();
        ArrayList<ImageRatioBean> imageRatioList = new MasterImageRationOperation().getImageRatioList();
        ArrayList<String> imageRatioNameList = new ArrayList<String>();
        ArrayList<ImageRatioBean> updateRatioList = new ArrayList<ImageRatioBean>();
        for(ImageRatioBean ratioBean : imageRatioList) 
            imageRatioNameList.add(ratioBean.getImageName().trim());
        
        ImageRatioBean imageRatioBean = null;
        for(QuestionBean questionBean : totalQuesList) {
            if(questionBean.isIsQuestionAsImage()) {
                if(!imageRatioNameList.contains(questionBean.getQuestionImagePath().trim())) {
                    imageRatioBean = new ImageRatioBean();
                    imageRatioBean.setImageName(questionBean.getQuestionImagePath().trim());
                    imageRatioBean.setViewDimention(1.0);
                    updateRatioList.add(imageRatioBean);
                }
            }
            if(questionBean.isIsOptionAsImage()) {
                if(!imageRatioNameList.contains(questionBean.getOptionImagePath().trim())) {
                    imageRatioBean = new ImageRatioBean();
                    imageRatioBean.setImageName(questionBean.getOptionImagePath().trim());
                    imageRatioBean.setViewDimention(1.0);
                    updateRatioList.add(imageRatioBean);
                }
            }
            if(questionBean.isIsHintAsImage()) {
                if(!imageRatioNameList.contains(questionBean.getHintImagePath().trim())) {
                    imageRatioBean = new ImageRatioBean();
                    imageRatioBean.setImageName(questionBean.getHintImagePath().trim());
                    imageRatioBean.setViewDimention(1.0);
                    updateRatioList.add(imageRatioBean);
                }
            }
        }
        
        ArrayList<QuestionBean> totalPaperQuesList = new QuestionPaperOperation().getPreviousYearPaperQuestionList();
        String masterImgName = null;
        for(QuestionBean questionBean : totalPaperQuesList) {
            if(questionBean.isIsQuestionAsImage()) {
                masterImgName = getMasterImageName(questionBean.getQuestionImagePath().trim());
                if(!imageRatioNameList.contains(masterImgName)) {
                    imageRatioBean = new ImageRatioBean();
                    imageRatioBean.setImageName(masterImgName);
                    imageRatioBean.setViewDimention(1.0);
                    updateRatioList.add(imageRatioBean);
                }
            }
            if(questionBean.isIsOptionAsImage()) {
                masterImgName = getMasterImageName(questionBean.getOptionImagePath().trim());
                if(!imageRatioNameList.contains(masterImgName)) {
                    imageRatioBean = new ImageRatioBean();
                    imageRatioBean.setImageName(masterImgName);
                    imageRatioBean.setViewDimention(1.0);
                    updateRatioList.add(imageRatioBean);
                }
            }
            if(questionBean.isIsHintAsImage()) {
                masterImgName = getMasterImageName(questionBean.getHintImagePath().trim());
                if(!imageRatioNameList.contains(masterImgName)) {
                    imageRatioBean = new ImageRatioBean();
                    imageRatioBean.setImageName(masterImgName);
                    imageRatioBean.setViewDimention(1.0);
                    updateRatioList.add(imageRatioBean);
                }
            }
        }
        
        updateRatioList.addAll(imageRatioList);
        imageRatioNameList.clear();
        System.out.println("Size:"+updateRatioList.size());
        for(ImageRatioBean bean : updateRatioList) 
            imageRatioNameList.add(bean.getImageName());
        
        for(ImageRatioBean ratioBean : imageRatioList) 
            imageRatioNameList.add(ratioBean.getImageName().trim());
        
        imageRatioList.clear();
        int index = 0;
        for(QuestionBean questionBean : totalQuesList) {
            if(questionBean.isIsQuestionAsImage()) {
                index = imageRatioNameList.indexOf(questionBean.getQuestionImagePath().trim());
                imageRatioList.add(updateRatioList.get(index));
            }
            if(questionBean.isIsOptionAsImage()) {
                index = imageRatioNameList.indexOf(questionBean.getOptionImagePath().trim());
                imageRatioList.add(updateRatioList.get(index));
            }
            if(questionBean.isIsHintAsImage()) {
                index = imageRatioNameList.indexOf(questionBean.getHintImagePath().trim());
                imageRatioList.add(updateRatioList.get(index));
            }
        }
        
        for(QuestionBean questionBean : totalPaperQuesList) {
            if(questionBean.isIsQuestionAsImage()) {
                masterImgName = getMasterImageName(questionBean.getQuestionImagePath().trim());
                index = imageRatioNameList.indexOf(masterImgName);
                imageRatioList.add(updateRatioList.get(index));
            }
            if(questionBean.isIsOptionAsImage()) {
                masterImgName = getMasterImageName(questionBean.getOptionImagePath().trim());
                index = imageRatioNameList.indexOf(masterImgName);
                imageRatioList.add(updateRatioList.get(index));
            }
            if(questionBean.isIsHintAsImage()) {
                masterImgName = getMasterImageName(questionBean.getHintImagePath().trim());
                index = imageRatioNameList.indexOf(masterImgName);
                imageRatioList.add(updateRatioList.get(index));
            }
        }
        System.out.println("After Sorting Size:"+imageRatioList.size());
//        for(ImageRatioBean bean : imageRatioList) {
//            System.out.println(bean.getImageName().trim()+"  "+bean.getViewDimention());
//        }
        
        if(addMasterRatio(imageRatioList))
            JOptionPane.showMessageDialog(null, "Image Ratio Updated SuccessFully.");
        else
            JOptionPane.showMessageDialog(null, "Error in Image Ratio Updation.");
    }
    
    private String getMasterImageName(String imgName) {
        return "masterI" + imgName.substring(1, imgName.length());
    } 
    
    private ArrayList<QuestionBean> getMasterQuestionList() {
        ArrayList<QuestionBean> returnList = null;
        QuestionBean bean = null;
        try {
            String query = "SELECT * FROM MASTER_QUESTION_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
                
            while (rs.next()) {
                bean = new QuestionBean();
                bean.setQuestionId(rs.getInt(1));                bean.setQuestion(rs.getString(2));
                bean.setOptionA(rs.getString(3));                bean.setOptionB(rs.getString(4));
                bean.setOptionC(rs.getString(5));                bean.setOptionD(rs.getString(6));
                bean.setAnswer(rs.getString(7));                 bean.setHint(rs.getString(8));
                bean.setLevel(rs.getInt(9));                     bean.setSubjectId(rs.getInt(10));
                bean.setChapterId(rs.getInt(11));                bean.setTopicId(rs.getInt(12));
                bean.setIsQuestionAsImage(rs.getBoolean(13));    bean.setQuestionImagePath(rs.getString(14));
                bean.setIsOptionAsImage(rs.getBoolean(15));      bean.setOptionImagePath(rs.getString(16));
                bean.setIsHintAsImage(rs.getBoolean(17));        bean.setHintImagePath(rs.getString(18));
                bean.setAttempt(rs.getInt(19));                  bean.setType(rs.getInt(20));
                bean.setYear(rs.getString(21));                  bean.setSelected(false);

                if(returnList == null)
                    returnList = new ArrayList<QuestionBean>();

                returnList.add(bean);
            }
        } catch (Exception ex) {
            returnList = null;
            ex.printStackTrace();
        }
        
        return returnList;
    }
    
    private boolean addMasterRatio(ArrayList<ImageRatioBean> updateRatioList) {
        boolean returnValue = false;
        try {
            String query = "DELETE FROM MASTER_IMAGERATIO";
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
            query = "INSERT INTO MASTER_IMAGERATIO VALUES(?,?)";
            for(ImageRatioBean imageRatioBean : updateRatioList) {
                ps = conn.prepareStatement(query);
                ps.setString(1, imageRatioBean.getImageName().trim());
                ps.setDouble(2, imageRatioBean.getViewDimention());
                ps.executeUpdate();
            }
            returnValue = true;
        } catch (Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        }
        return returnValue;
    }
    
    public static void main(String[] args) {
        new AddRatioManager().setRatio();
    }
}
