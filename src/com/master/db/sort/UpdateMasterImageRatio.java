/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.master.db.sort;

import com.bean.ImageRatioBean;
import com.bean.QuestionBean;
import com.db.DbConnection;
import com.db.operations.MasterImageRationOperation;
import com.db.operations.MasterQuestionOperation;
import com.db.operations.QuestionPaperOperation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class UpdateMasterImageRatio {
    private ArrayList<ImageRatioBean> masterImageRatioList;
    private ArrayList<String> ImageRatioList;

    public UpdateMasterImageRatio() {
        masterImageRatioList = new MasterImageRationOperation().getImageRatioList();
        ImageRatioList = new ArrayList<String>();
        for(ImageRatioBean ratioBean : masterImageRatioList)
            ImageRatioList.add(ratioBean.getImageName());
    }
    
    public void updateRemainingRatio() {
        ArrayList<ImageRatioBean> updateRatioList = new ArrayList<ImageRatioBean>();
        ArrayList<QuestionBean> masterQuestionList = new MasterQuestionOperation().getQuestuionsList();
        ImageRatioBean imageRatioBean = null;
        int cnt1 = 0;
        for(QuestionBean questionBean : masterQuestionList) {
            if(questionBean.isIsQuestionAsImage()) {
                imageRatioBean = getImageRatioBean(questionBean.getQuestionImagePath());
                if(imageRatioBean == null) {
                    imageRatioBean = new ImageRatioBean();
                    imageRatioBean.setImageName(questionBean.getQuestionImagePath().trim());
                    imageRatioBean.setViewDimention(1.0);
                    updateRatioList.add(imageRatioBean);
                    cnt1++;
                } else {
                    updateRatioList.add(imageRatioBean);
                }
            }
            
            if(questionBean.isIsOptionAsImage()) {
                imageRatioBean = getImageRatioBean(questionBean.getOptionImagePath());
                if(imageRatioBean == null) {
                    imageRatioBean = new ImageRatioBean();
                    imageRatioBean.setImageName(questionBean.getOptionImagePath().trim());
                    imageRatioBean.setViewDimention(1.0);
                    updateRatioList.add(imageRatioBean);
                    cnt1++;
                } else {
                    updateRatioList.add(imageRatioBean);
                }
                /*if(!ImageRatioList.contains(questionBean.getOptionImagePath().trim())) {
                    imageRatioBean = new ImageRatioBean();
                    imageRatioBean.setImageName(questionBean.getOptionImagePath().trim());
                    imageRatioBean.setViewDimention(1.0);
                    updateRatioList.add(imageRatioBean);
                    cnt1++;
                } else {
                    int index = ImageRatioList.indexOf(questionBean.getOptionImagePath().trim());
                    updateRatioList.add(masterImageRatioList.get(index));
                }*/
            }
            
            if(questionBean.isIsHintAsImage()) {
                imageRatioBean = getImageRatioBean(questionBean.getHintImagePath());
                if(imageRatioBean == null) {
                    imageRatioBean = new ImageRatioBean();
                    imageRatioBean.setImageName(questionBean.getHintImagePath().trim());
                    imageRatioBean.setViewDimention(1.0);
                    updateRatioList.add(imageRatioBean);
                    cnt1++;
                } else {
                    updateRatioList.add(imageRatioBean);
                }
            } 
        }
        
        ArrayList<QuestionBean> paperQuestionList = new QuestionPaperOperation().getPreviousYearPaperQuestionList();
        for(QuestionBean questionBean : paperQuestionList) {
            if(questionBean.isIsQuestionAsImage()) {
                imageRatioBean = getImageRatioBean("masterI"+questionBean.getQuestionImagePath().substring(1, questionBean.getQuestionImagePath().length()));
                if(imageRatioBean == null) {
                    imageRatioBean = new ImageRatioBean();
                    imageRatioBean.setImageName(questionBean.getQuestionImagePath().trim());
                    imageRatioBean.setViewDimention(1.0);
                    updateRatioList.add(imageRatioBean);
                    cnt1++;
                } else {
                    updateRatioList.add(imageRatioBean);
                }
            }
            
            if(questionBean.isIsOptionAsImage()) {
                imageRatioBean = getImageRatioBean("masterI"+questionBean.getOptionImagePath().substring(1, questionBean.getOptionImagePath().length()));
                if(imageRatioBean == null) {
                    imageRatioBean = new ImageRatioBean();
                    imageRatioBean.setImageName(questionBean.getOptionImagePath().trim());
                    imageRatioBean.setViewDimention(1.0);
                    updateRatioList.add(imageRatioBean);
                    cnt1++;
                } else {
                    updateRatioList.add(imageRatioBean);
                }
            }
            
            if(questionBean.isIsHintAsImage()) {
                imageRatioBean = getImageRatioBean("masterI"+questionBean.getHintImagePath().substring(1, questionBean.getHintImagePath().length()));
                if(imageRatioBean == null) {
                    imageRatioBean = new ImageRatioBean();
                    imageRatioBean.setImageName(questionBean.getHintImagePath().trim());
                    imageRatioBean.setViewDimention(1.0);
                    updateRatioList.add(imageRatioBean);
                    cnt1++;
                } else {
                    updateRatioList.add(imageRatioBean);
                }
            } 
        }
        System.out.println("Size:"+updateRatioList.size());
        insertMasterImageRation(updateRatioList);
    }
    
    private void insertMasterImageRation(ArrayList<ImageRatioBean> updateRatioList) {
        try {
            Connection conn = new DbConnection().getConnection();
            String query = "DELETE FROM MASTER_IMAGERATIO";
            PreparedStatement ps = conn.prepareStatement(query);
            ps.executeUpdate();
            
            query = "INSERT INTO MASTER_IMAGERATIO VALUES(?,?)";
            ps = conn.prepareStatement(query);
            for(ImageRatioBean imageRatioBean : updateRatioList) {
                ps.setString(1, imageRatioBean.getImageName());
                ps.setDouble(2, imageRatioBean.getViewDimention());
                ps.executeUpdate();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
        }
    }
    
    private ImageRatioBean getImageRatioBean(String imagePath) {
        ImageRatioBean returnBean = null;
        for(ImageRatioBean bean : masterImageRatioList) {
            if(imagePath.equalsIgnoreCase(bean.getImageName())) {
                returnBean = bean;
                break;
            }
        }
        return returnBean;
    }
    
    public static void main(String[] args) {
        new UpdateMasterImageRatio().updateRemainingRatio();
    }
}
