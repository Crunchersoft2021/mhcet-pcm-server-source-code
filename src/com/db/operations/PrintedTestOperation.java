/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.PrintedTestBean;
import com.bean.SubjectBean;
import com.bean.ViewTestBean;
import com.db.DbConnection;
import com.id.operations.NewIdOperation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Aniket
 */
public class PrintedTestOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public ArrayList<PrintedTestBean> getPintedTests() {
        ArrayList<PrintedTestBean> returnList = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM PRINTED_TEST_INFO";

            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            
            PrintedTestBean printedTestBean = null;
            
            while(rs.next()) {
                
                printedTestBean = new PrintedTestBean();
                printedTestBean.setPrintedTestId(rs.getInt(1));
                printedTestBean.setTestName(rs.getString(2));
                printedTestBean.setQuesIds(rs.getString(3));
                printedTestBean.setTotalQues(rs.getInt(4));
                printedTestBean.setQuesType(rs.getString(5));
                printedTestBean.setSubjectIds(rs.getString(6));
                printedTestBean.setTestDateTime(rs.getTimestamp(7));
                printedTestBean.setQuestionListObject(rs.getBytes(8));
                
                if(returnList == null)
                    returnList = new ArrayList<PrintedTestBean>();
                
                returnList.add(printedTestBean);
            }
        } catch(Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
           // sqlClose();
        }
        return returnList;
    }
    
    public boolean insertPrintedTest(PrintedTestBean printedTestBean) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "INSERT INTO PRINTED_TEST_INFO VALUES(?,?,?,?,?,?,?,?)";
            ps = conn.prepareStatement(query);
            
            ps.setInt(1, printedTestBean.getPrintedTestId());
            ps.setString(2, printedTestBean.getTestName());
            ps.setString(3, printedTestBean.getQuesIds());
            ps.setInt(4, printedTestBean.getTotalQues());
            ps.setString(5, printedTestBean.getQuesType());
            ps.setString(6,printedTestBean.getSubjectIds());
            ps.setTimestamp(7, printedTestBean.getTestDateTime());
            ps.setBytes(8, printedTestBean.getQuestionListObject());
            
            ps.executeUpdate();

            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnValue;
    }
    
    public boolean deletePrintedTest(PrintedTestBean printedTestBean) {
        boolean returnValue = false;
        int lastId = new NewIdOperation().getNewId("PRINTED_TEST_INFO");
        lastId -= 1;
        try {
            int currentTestId = printedTestBean.getPrintedTestId();
            conn = new DbConnection().getConnection();
            String query = "DELETE FROM PRINTED_TEST_INFO WHERE PRINTED_TEST_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, currentTestId);
            ps.executeUpdate();
            if(lastId != currentTestId) {
                for(int i=currentTestId; i<lastId; i++) {
                    query = "UPDATE PRINTED_TEST_INFO SET PRINTED_TEST_ID = ? WHERE PRINTED_TEST_ID = ?";
                    ps = conn.prepareStatement(query);
                    ps.setInt(1, i);
                    ps.setInt(2, i + 1);
                    ps.executeUpdate();
                }
            }
            
            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    public boolean deleteTestList(ArrayList<PrintedTestBean> deleteTestList) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "DELETE FROM PRINTED_TEST_INFO WHERE PRINTED_TEST_ID = ?";
            for(PrintedTestBean printedTestBean : deleteTestList) {
                ps = conn.prepareStatement(query);
                ps.setInt(1, printedTestBean.getPrintedTestId());
                ps.executeUpdate();
            }
            query = "UPDATE PRINTED_TEST_INFO SET PRINTED_TEST_ID = ? WHERE PRINTED_TEST_ID = ?";
            int totalDeleted = deleteTestList.size();
            for(int i=1;i<=50;i++) {
                ps = conn.prepareStatement(query);
                ps.setInt(1, i);
                ps.setInt(2, i + totalDeleted);
                ps.executeUpdate();
            }
            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    public ArrayList<ViewTestBean> getViewPrintedTestList() {
        ArrayList<PrintedTestBean> printedTestList = getPintedTests();
        
        ArrayList<ViewTestBean> returnList = null;
        ArrayList<SubjectBean> subjectList = new SubjectOperation().getSubjectsList();
        ViewTestBean viewTestBean = null;
        if(printedTestList != null) {
            Collections.sort(printedTestList);
            String titleName;
            for(PrintedTestBean printedTestBean : printedTestList) {
                titleName = "";
                viewTestBean = new ViewTestBean();
                
                if(printedTestBean.getQuesType().equalsIgnoreCase("ChapterWise"))
                    titleName = "Chapter Wise";
                else if(printedTestBean.getQuesType().equalsIgnoreCase("SubjectWise"))
                    titleName = "Subject Wise";
                else if(printedTestBean.getQuesType().equalsIgnoreCase("GroupWise"))
                    titleName = "Group Wise";
                else if(printedTestBean.getQuesType().equalsIgnoreCase("YearWise"))
                    titleName = "Year Wise";
                viewTestBean.setTitleName(titleName);
                viewTestBean.setSelectedSubjects(getSelectedSubjects(subjectList, printedTestBean.getSubjectIds()));
                viewTestBean.setSaveTime(getStringDateTime(printedTestBean.getTestDateTime()));
                viewTestBean.setTotalQuestions(printedTestBean.getTotalQues());
                viewTestBean.setPrintedTestBean(printedTestBean);

                if(returnList == null)
                    returnList = new ArrayList<ViewTestBean>();

                returnList.add(viewTestBean);
            }
        }
        return returnList;
    }
    
    private String getSelectedSubjects(ArrayList<SubjectBean> subjectList,String subjectIds) {
        String returnString = "";
        String[] subjectIdArray = subjectIds.split(",");
        
        int subId = 0;
        if(subjectIdArray.length == 1) {
            returnString = "Subject : ";
            subId = Integer.parseInt(subjectIdArray[0]);
            for(SubjectBean subjectBean : subjectList) {
                if(subjectBean.getSubjectId() == subId) {
                    returnString += subjectBean.getSubjectName();
                    break;
                }
            }
        } else {
            returnString = "Group : ";
            for(String subjectId : subjectIdArray) {
                subId = Integer.parseInt(subjectId);
                for(SubjectBean subjectBean : subjectList) {
                    if(subjectBean.getSubjectId() == subId) {
                        returnString += subjectBean.getSubjectName().substring(0, 1);
                        break;
                    }
                }
            }
        }
        return returnString;
    }
    
    private String getStringDateTime(Timestamp timestamp) {
        String returnDate = new SimpleDateFormat("dd-MM-yy hh:mm a").format(timestamp.getTime());
        return returnDate;
    }
    
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
