/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.Server.IPAddress;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aniket
 */
public class LogOutOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    private Statement stmt = null;
    
    
    public boolean logout(int rollNo) {
         boolean Status = false;
         conn = new DbConnection().getConnection();
         String ClientPCName=new IPAddress().getIPAddress("192.168.2");
        try {
             stmt = conn.createStatement();
            stmt.execute("update Client_Info set status='false' where CLIENTPCNAME='" + ClientPCName+ "'");
            return Status=true;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(LogOutOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return Status;
    }
}
