/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.ProductSubjectsBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Aniket
 */
public class ProductSubjectsOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public ProductSubjectsBean getProductSubjectsBean(int productId) {
        ProductSubjectsBean returnBean = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM PRODUCT_SUBJECT_INFO WHERE PRD_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, productId);
            rs = ps.executeQuery();
            while (rs.next()) {
                returnBean = new ProductSubjectsBean();
                returnBean.setProductSubjectId(rs.getInt(1));
                returnBean.setProductId(rs.getInt(2));
                returnBean.setProductSubjects(rs.getString(3));
            }
        } catch (SQLException ex) {
            returnBean = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnBean;
    }
    
    private void sqlClose() {
        try {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
