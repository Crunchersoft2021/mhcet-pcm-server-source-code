/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

import java.io.Serializable;

/**
 *
 * @author Aniket
 */
public class ViewChapterBean implements Serializable{
    private int chapterId;
    private String chapterName;
    
    private ChapterBean chapterBean;
    private int easy;
    private int medium;
    private int hard;
    private int numerical;
    private int theory;
    private int notUsed;
    private int used;
    private int asked;
    private int notAsked;
    private int hint;
    private int noHint;
    private int totalQue;

    public int getTotalQue() {
        return totalQue;
    }

    public void setTotalQue(int totalQue) {
        this.totalQue = totalQue;
        
    }

    public int getNoHint() {
        return noHint;
    }

    public void setNoHint(int noHint) {
        this.noHint = noHint;
    }

    public int getEasy() {
        return easy;
    }

    public void setEasy(int easy) {
        this.easy = easy;
    }

    public int getMedium() {
        return medium;
    }

    public void setMedium(int medium) {
        this.medium = medium;
    }

    public int getHard() {
        return hard;
    }

    public void setHard(int hard) {
        this.hard = hard;
    }

    public int getNumerical() {
        return numerical;
    }

    public void setNumerical(int numerical) {
        this.numerical = numerical;
    }

    public int getTheory() {
        return theory;
    }

    public void setTheory(int theory) {
        this.theory = theory;
    }

    public int getNotUsed() {
        return notUsed;
    }

    public void setNotUsed(int notUsed) {
        this.notUsed = notUsed;
    }

    public int getUsed() {
        return used;
    }

    public void setUsed(int used) {
        this.used = used;
    }

    public int getAsked() {
        return asked;
    }

    public void setAsked(int asked) {
        this.asked = asked;
    }

    public int getNotAsked() {
        return notAsked;
    }

    public void setNotAsked(int notAsked) {
        this.notAsked = notAsked;
    }

    public int getHint() {
        return hint;
    }

    public void setHint(int hint) {
        this.hint = hint;
    }

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public ChapterBean getChapterBean() {
        return chapterBean;
    }

    public void setChapterBean(ChapterBean chapterBean) {
        this.chapterBean = chapterBean;
    }
}
