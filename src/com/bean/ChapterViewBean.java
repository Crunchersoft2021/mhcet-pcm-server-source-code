/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class ChapterViewBean {
    private int chapterId;
    private String chapterName;
    private MasterSubjectBean subjectBean;
    private int totalQuesCount;
    private GroupBean groupBean;

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public MasterSubjectBean getSubjectBean() {
        return subjectBean;
    }

    public void setSubjectBean(MasterSubjectBean subjectBean) {
        this.subjectBean = subjectBean;
    }

    public int getTotalQuesCount() {
        return totalQuesCount;
    }

    public void setTotalQuesCount(int totalQuesCount) {
        this.totalQuesCount = totalQuesCount;
    }

    public GroupBean getGroupBean() {
        return groupBean;
    }

    public void setGroupBean(GroupBean groupBean) {
        this.groupBean = groupBean;
    }
}
